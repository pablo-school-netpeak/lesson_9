<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lesson extends MY_Controller {

  public function __construct()
  {
          parent::__construct();
          $this->load->model('lesson_model');
  }
	public function index()
	{
    $this->load->helper('lesson_helper');

    echo newHelper_method('Try to test new helper');

    $posts =  $this->lesson_model->get_news();

    $data = array(
      'posts' => $posts,
    );

		$this->load->view('lesson/index_view', $data);
	}

  public function showNews($id = NULL)
  {
    $news_item =  $this->lesson_model->get_news($id);

    $data = array(
      'news_item' => $news_item,
    );

    $this->load->view('lesson/showNews_view', $data);
  }

  public function insertNews($title = '')
  {
    $this->lesson_model->insert_news($title);

  }

  public function deleteNews($id = NULL)
  {
    $this->lesson_model->delete_news($id);
  }

  public function updateNews($id = NULL, $title = '')
  {

    $this->lesson_model->update_news($id, $title);
  }

}
